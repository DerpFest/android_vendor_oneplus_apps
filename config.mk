PRODUCT_SOONG_NAMESPACES += \
    vendor/oneplus/apps

PRODUCT_PACKAGES += \
    OnePlusCameraService

TARGET_SHIPS_OOSCAM ?= true
TARGET_SHIPS_OOSGALLERY ?= true

ifeq ($(TARGET_SHIPS_OOSCAM),true)
PRODUCT_PACKAGES += \
    OnePlusCamera
endif

ifeq ($(TARGET_SHIPS_OOSGALLERY),true)
PRODUCT_PACKAGES += \
    OnePlusGallery
endif

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/etc/permissions/com.oneplus.camera.service.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.oneplus.camera.service.xml \
    $(LOCAL_PATH)/etc/sysconfig/hiddenapi-package-whitelist-oem.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/hiddenapi-package-whitelist-oem.xml \
    $(LOCAL_PATH)/system_ext/etc/permissions/privapp-permissions-oem-system_ext.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/privapp-permissions-oem-system_ext.xml
